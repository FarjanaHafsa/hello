<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\PersonController;
use App\Http\Controllers\CourseController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

    return view('welcome');
});
Route::get('/create', [CategoryController::class, 'create'])->name('create');
Route::post('/store', [CategoryController::class, 'store'])->name('store');
Route::get('/list', [CategoryController::class, 'list'])->name('list');
Route::get('/delete/{id}', [CategoryController::class, 'destroy'])->name('delete');

Route::get('/edit/{id}', [CategoryController::class, 'edit'])->name('edit');
Route::patch('/update/{id}', [CategoryController::class, 'update'])->name('update');

//Route::get('/set',[ProductController::class,'index'])->name('view');

//
Route::get('/person', [PersonController::class, 'create'])->name('person');
Route::post('/person-store', [PersonController::class, 'store'])->name('person-store');
Route::get('/person-edit/{id}', [PersonController::class, 'edit'])->name('person-edit');
Route::post('/person-update/{id}', [PersonController::class, 'update'])->name('person-update');
Route::get('/person-list', [PersonController::class, 'list'])->name('person-list');
Route::get('/person-delete/{id}', [CategoryController::class, 'destroy'])->name('person-delete');
//

Route::get('/course', [CourseController::class, 'create'])->name('course-create');
Route::post('/course_store', [CourseController::class, 'store'])->name('course');
Route::get('/course_list', [CourseController::class, 'index'])->name('course-list');

Route::get('/course/{id}', [CourseController::class, 'edit'])->name('course-edit');
Route::get('/course/delete/{id}', [CourseController::class, 'destroy'])->name('course-delete');
Route::post('/course/update/{id}', [CourseController::class, 'update'])->name('course_update');
