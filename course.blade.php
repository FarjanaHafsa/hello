<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <title></title>
    <style>
    </style>
</head>

<body>
    <h1>Hello, world!</h1>
    <div class="container justify-content-lg-center">
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        <form action="{{route('course')}}" method="post">
            @csrf
            <div class="form-group">
                <label>title</label>
                <input type="text" name="title" id="title" class="form-control ">
            </div>
            <div class="form-group">
                <label>category</label>
                <br>
                <select name="category[]" id="category" multiple>
                    <option value="web design">web design</option>
                    <option value="web development">web development</option>
                    <option value="SEO">SEO</option>
                </select>
            </div>
            <br>
            <div class="form-group">
                <label>Type</label>
                <input type="radio" name="type" id="online" value="online">online
                <input type="radio" name="type" id="offline" value="offline">offline
                <br>
            </div>
            <div class="form-group">
                <label>Technology</label>
                <input type="checkbox" name="technology[]" value="HTML">Html
                <input type="checkbox" name="technology[]" value="CSS">CSS
                <input type="checkbox" name="technology[]" value="PHP">PHP
                <br>
                <div class="form-group">
                    <label>Duration</label>
                    <input type="text" name="duration" class="form-control">
                    <br>
                </div>
                <div class="form-group">
                    <label>Start-date</label>
                    <input type="date" name="start_date">
                    <br>
                </div>
                <input type="submit" value="submit" class="btn btn-primary">

        </form>
    </div>
    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
    -->
</body>

</html>