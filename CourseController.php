<?php

namespace App\Http\Controllers;

use App\Models\Course;
use Illuminate\Http\Request;
use app\http\Requests\CategoryRequest;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $course = Course::all();
        return view('course-list', compact('course'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('course');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryRequest $request)
    {


        $category = $request['category'];
        $technology = $request['technology'];

        $request['technology'] = implode(',', $technology);
        $request['category'] = implode(',', $category);


        $data = ['title' => $request['title'], 'category' => $request['category'], 'type' => $request['type'], 'technology' => $request['technology'], 'duration' => $request['duration'], 'start_date' => $request['start_date'],];

        Course::create($data);

        //return redirect()->route('course-list');
        return redirect()->route('course-list')->withMessage('Inserted sucessfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function show(Course $course)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function edit(Course $course, $id)
    {
        $course = Course::find($id);
        return view('course-edit', Compact('course'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryRequest $request, Course $course, $id)
    {

        $course = Course::find($id);

        $category = $request['category'];
        $technology = $request['technology'];

        $request['technology'] = implode(',', $technology);
        $request['category'] = implode(',', $category);


        $data = ['title' => $request['title'], 'category' => $request['category'], 'type' => $request['type'], 'technology' => $request['technology'], 'duration' => $request['duration'], 'start_date' => $request['start_date'],];

        $course->update($data);

        return redirect()->route('course-list')->withMessage('Updated sucessfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function destroy(Course $course, $id)
    {
        $course = Course::find($id);
        $course->delete();
        return redirect()->route('course-delete')->withMessage('Deleted sucessfully');
    }
}
