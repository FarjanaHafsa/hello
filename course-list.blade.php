<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <title>Document</title>
</head>

<body>

    @if(session('message'))
    <div class="text-sucess">{{ session('message') }}</div>
    @endif
    <div class="container">
        <div class="button"><a href="{{route('course-create')}}" class="btn btn-primary">ADD</a></div>

        <h3>Hello Students</h3>
        <table class="table table-dark table-striped">
            <tr>
                <th>ID</th>
                <th>Title</th>
                <th>Category</th>
                <th>Type</th>
                <th>Technology</th>
                <th>Duration</th>
                <th>Start-date</th>
                <th>Action</th>
            </tr>
            @foreach($course as $row)
            <tr>
                <td>{{$loop->iteration}}</td>
                <td> {{ $row->title }}</td>
                <td> {{ $row->category }}</td>
                <td> {{ $row->type }}</td>
                <td> {{ $row->technology }}</td>
                <td> {{ $row->duration }}</td>
                <td> {{ $row->start_date }}</td>
                <td><a href="{{route('course-edit',$row->id)}}" class="btn btn-primary">edit </a>
                    <a href="{{route('course-delete',$row->id)}}" class="btn btn-danger" value="delete">delete
                </td>

            </tr>
            @endforeach
        </table>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>

</body>

</html>